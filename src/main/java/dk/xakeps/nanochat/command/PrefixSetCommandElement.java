/*
 * This file is part of NanoChat.
 *
 * NanoChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NanoChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NanoChat.  If not, see <https://www.gnu.org/licenses/>.
 */
package dk.xakeps.nanochat.command;

import dk.xakeps.nanochat.config.ConfigManager;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.function.Consumer;

public class PrefixSetCommandElement implements CommandExecutor {
    public static CommandElement ARGUMENT = GenericArguments.text(Text.of("prefix"), TextSerializers.FORMATTING_CODE, true);

    private final ConfigManager configManager;
    private final Consumer<Text> textSetter;
    private final Text chatType;

    public PrefixSetCommandElement(ConfigManager configManager, Consumer<Text> textSetter, Text chatType) {
        this.configManager = configManager;
        this.textSetter = textSetter;
        this.chatType = chatType;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        Text prefix = args.<Text>getOne(ARGUMENT.getKey()).get();
        textSetter.accept(prefix);
        configManager.saveConfig();
        src.sendMessage(Text.of(TextColors.GREEN, "Prefix for ", TextColors.GOLD, chatType,
                TextColors.GREEN, " was set, new prefix: ", TextColors.RESET, prefix));
        return CommandResult.success();
    }
}
