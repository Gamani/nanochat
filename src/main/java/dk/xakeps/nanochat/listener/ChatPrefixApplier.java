package dk.xakeps.nanochat.listener;

import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.TextTemplate;
import org.spongepowered.api.text.transform.SimpleTextTemplateApplier;

import java.util.Objects;

final class ChatPrefixApplier extends SimpleTextTemplateApplier {
    private static final String PARAM_CHAT_PREFIX = "chatPrefix";
    ChatPrefixApplier(Text prefix) {
        super(TextTemplate.of(TextTemplate.arg(PARAM_CHAT_PREFIX)));
        Objects.requireNonNull(prefix, "Prefix is null");
        setParameter(PARAM_CHAT_PREFIX, prefix);
    }
}
