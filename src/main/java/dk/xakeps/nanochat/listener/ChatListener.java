package dk.xakeps.nanochat.listener;

import dk.xakeps.nanochat.config.Config;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.event.message.MessageEvent;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.*;
import org.spongepowered.api.text.TextElement;
import org.spongepowered.api.text.channel.MessageChannel;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.transform.SimpleTextTemplateApplier;
import org.spongepowered.api.world.Locatable;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ChatListener implements EventListener<MessageChannelEvent.Chat> {
    public static final String PERM_CAN_SEE_LOCAL = "nanochat.chat.canseelocal";
    private static final Pattern EXCL_POINT_REPLACE_PATTERN = Pattern.compile("^!");
    private final Supplier<Config> configSupplier;

    public ChatListener(Supplier<Config> configSupplier) {
        this.configSupplier = configSupplier;
    }

    @Override
    public void handle(@Nonnull MessageChannelEvent.Chat event) {
        Config config = configSupplier.get();
        if (!config.isEnabled()
                || !(event.getCause().root() instanceof Player)) {
            return;
        }
        Player player = (Player) event.getCause().root();

        boolean beginsWithExclPoint = event.getRawMessage().toPlain().startsWith("!");
        boolean global = beginsWithExclPoint && player.hasPermission("nanochat.chat.global");
        Text prefix = config.getGlobalPrefix();
        if (!global) {
            int rangeSquared = config.getRange() * config.getRange();
            prefix = config.getLocalPrefix();

            Optional<MessageChannel> eventChannelOpt = event.getChannel();
            MessageChannel channel;
            // If some plugins changed MessageChannel
            if (eventChannelOpt.isPresent() && !eventChannelOpt.get().equals(event.getOriginalChannel())) {
                channel = () -> eventChannelOpt.get().getMembers().stream()
                        .filter(receiver -> filterByPermissionOrRangePredicate(player, receiver, rangeSquared))
                        .collect(Collectors.toSet());
            } else { // If MessageChannel was not changed
                channel = MessageChannel.combined(
                        () -> getNearbyPlayers(player, rangeSquared),
                        MessageChannel.permission(PERM_CAN_SEE_LOCAL)
                );
            }
            event.setChannel(MessageChannel.combined(channel, MessageChannel.TO_CONSOLE));
        }

        event.getFormatter().getHeader().insert(0, new ChatPrefixApplier(prefix));

        if (config.isModifyBody() && beginsWithExclPoint) {
            // Do not modify any data passed by other plugins. Modify only player's message itself.
            // If message was changed by another plugin, respect that change and only remove `!` in the beginning
            // Replace only if `!` was in original message sent by user. So this will respect, if plugin added `!` in the beginning
            for (SimpleTextTemplateApplier applier : event.getFormatter().getBody()) {
                TextElement parameter = applier.getParameter(MessageEvent.PARAM_MESSAGE_BODY);
                if (parameter != null) { // replace only if applier has MessageEvent.PARAM_MESSAGE_BODY
                    Text.Builder t = Text.builder();
                    parameter.applyTo(t); // Text.of() does not respect TextElement
                    Text replace = t.build().replace(EXCL_POINT_REPLACE_PATTERN, Text.of(), false);
                    applier.setParameter(MessageEvent.PARAM_MESSAGE_BODY, replace);
                    break;
                }
            }
        }
    }

    private static Set<MessageReceiver> getNearbyPlayers(Player p, int rangeSquared) {
        return p.getWorld().getPlayers().stream()
                .filter(target -> target.getPosition().distanceSquared(p.getPosition()) <= rangeSquared)
                .collect(Collectors.toSet());
    }

    private static boolean isInRange(Player source, Locatable target, int rangeSquared) {
        return source.getWorld().equals(target.getWorld())
                && source.getPosition().distanceSquared(target.getLocation().getPosition()) <= rangeSquared;
    }

    private static boolean filterByPermissionOrRangePredicate(Player source, MessageReceiver receiver, int rangeSquared) {
        if (receiver instanceof Subject && ((Subject) receiver).hasPermission(ChatListener.PERM_CAN_SEE_LOCAL)) {
            return true;
        }

        if (receiver instanceof Locatable) {
            return isInRange(source, (Locatable) receiver, rangeSquared);
        }
        return false;
    }

}
